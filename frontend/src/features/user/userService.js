import axios from 'axios'

const API_URL = 'http://localhost:8080/api/users/me';


//get user Info
const getMyInfo = async (token) => {

   const config = {
      headers: {
         Authorization: `Bearer ${token}`
      }
   }

   const response = await axios.get(API_URL, config);

   return response.data
}

//change user password
const changeMyPassword = async (passwords, token) => {
 
   const config = {
      headers: {
         Authorization: `Bearer ${token}`
      }
   }
   const response = await axios.patch(API_URL, passwords, config);

   return response.data
}

//delete user 
const deleteMe = async (token) => {
 
   const config = {
      headers: {
         Authorization: `Bearer ${token}`
      }
   }
   const response = await axios.delete(API_URL, config);

   return response.data
}



const userService = {
   getMyInfo,
   changeMyPassword,
   deleteMe
}

export default userService
