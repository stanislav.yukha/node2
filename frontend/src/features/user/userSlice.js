import {createSlice, createAsyncThunk} from '@reduxjs/toolkit'
import userService from './userService'


const initialState = {
   userInfo: {
      _id: 'n/a',
      username: 'n/a',
      createdDate: 'n/a'
   },
   isSuccess: false,
   isError: false,
   isLoading: false,
   message: ''
}

// Get user info
export const getMyInfo = createAsyncThunk('user/getInfo', async(_, thunkAPI) => {
   try {
      const token = thunkAPI.getState().auth.user.jwt_token;
      return await userService.getMyInfo(token)
   } catch(err) {
      const message = (err.response && err.response.data && err.response.data.message) ||
       err.message || err.toString()
      return thunkAPI.rejectWithValue(message)
   }
}
)

// Get user password
export const changeMyPassword = createAsyncThunk('user/changePass', async(passwords, thunkAPI) => {
   try {
      const token = thunkAPI.getState().auth.user.jwt_token;
      return await userService.changeMyPassword(passwords,token)
   } catch(err) {
      const message = (err.response && err.response.data && err.response.data.message) ||
       err.message || err.toString()
      return thunkAPI.rejectWithValue(message)
   }
}
)

//Delete user
export const deleteMe = createAsyncThunk('user/delete', async(_, thunkAPI) => {
   try {
      const token = thunkAPI.getState().auth.user.jwt_token;
      return await userService.deleteMe(token)
   } catch(err) {
      const message = (err.response && err.response.data && err.response.data.message) ||
       err.message || err.toString()
      return thunkAPI.rejectWithValue(message)
   }
}
)





export const userSlice = createSlice({
   name: 'user',
   initialState,
   reducers: {
      reset: (state) => initialState,
   },
   extraReducers: (builder) => {
      builder
         .addCase(getMyInfo.pending, (state) => {
            state.isLoading = true
         })
         .addCase(getMyInfo.fulfilled, (state, action) => {
            state.isLoading = false
            state.isSuccess = true
            state.userInfo = action.payload.user;
         })
         .addCase(getMyInfo.rejected, (state, action) => {
            state.isLoading = false
            state.isError = true
            state.message = action.payload
         })
         .addCase(changeMyPassword.pending, (state) => {
            state.isLoading = true
         })
         .addCase(changeMyPassword.fulfilled, (state) => {
            state.isLoading = false
            state.isSuccess = true
         })
         .addCase(changeMyPassword.rejected, (state, action) => {
            state.isLoading = false
            state.isError = true
            state.message = action.payload
         })
         .addCase(deleteMe.pending, (state) => {
            state.isLoading = true
         })
         .addCase(deleteMe.fulfilled, (state) => {
            state.isLoading = false
            state.isSuccess = true
         })
         .addCase(deleteMe.rejected, (state, action) => {
            state.isLoading = false
            state.isError = true
            state.message = action.payload
         })
         
   }
})

export const {reset} = userSlice.actions
export default userSlice.reducer

