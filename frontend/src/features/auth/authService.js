import axios from 'axios'

const API_URL = 'http://localhost:8080/api/auth';
const REG_URL = API_URL + '/register';
const LOG_URL = API_URL + '/login';

//Register
export const register = async (userData) => {
   const response = await axios.post(REG_URL, userData);

   if(response.data) {
      localStorage.setItem('user', JSON.stringify(response.data));

   }
   return response.data;
}

//Login
export const login = async (userData) => {
   const response = await axios.post(LOG_URL, userData);

   if(response.data) {
      localStorage.setItem('user', JSON.stringify(response.data));

   }
   return response.data;
}

//Logout
export const logout = () => {
   localStorage.removeItem('user');
}

const authService = {
   register,
   logout,
   login
}

export default authService