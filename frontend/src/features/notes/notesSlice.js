import {createSlice, createAsyncThunk} from '@reduxjs/toolkit'
import notesService from './notesService'

const initialState = {
   notes: [],
   note: null,
   isSuccess: false,
   isError: false,
   isLoading: false,
   message: ''
}


// Get user notes

export const getNotes = createAsyncThunk('notes/getAll', async(queryParams, thunkAPI) => {
   try {
      const token = thunkAPI.getState().auth.user.jwt_token;
      return await notesService.getNotes(queryParams, token)
   } catch(err) {
      const message = (err.response && err.response.data && err.response.data.message) ||
       err.message || err.toString()
      return thunkAPI.rejectWithValue(message)
   }
}
)

// Get user note by id

export const getNote = createAsyncThunk('notes/getOne', async(id, thunkAPI) => {
   try {
      const token = thunkAPI.getState().auth.user.jwt_token;
      return await notesService.getNote(id, token)
   } catch(err) {
      const message = (err.response && err.response.data && err.response.data.message) ||
       err.message || err.toString()
      return thunkAPI.rejectWithValue(message)
   }
}
)




//Create new note
export const createNote = createAsyncThunk('notes/create', async (noteData, thunkAPI) => {
   try {
      const token = thunkAPI.getState().auth.user.jwt_token;
      return await notesService.createNote(noteData, token)
   } catch(err) {
      const message = (err.response && err.response.data && err.response.data.message) ||
       err.message || err.toString()
      return thunkAPI.rejectWithValue(message)
   }
}
)

//Delete note
export const deleteNote = createAsyncThunk('notes/delete', async (id, thunkAPI) => {
   try {
      const token = thunkAPI.getState().auth.user.jwt_token
      return await notesService.deleteNote(id, token)
   } catch(err) {
      const message = (err.response && err.response.data && err.response.data.message) ||
       err.message || err.toString()
      return thunkAPI.rejectWithValue(message)
   }
}
)


//  Check/uncheck 'completed' status of note by id
export const checkNote = createAsyncThunk('notes/check', async (id, thunkAPI) => {
     try {
       const token = thunkAPI.getState().auth.user.jwt_token
       return await notesService.checkNote(id, token)
     } catch(err) {
      const message = (err.response && err.response.data && err.response.data.message) ||
       err.message || err.toString()
      return thunkAPI.rejectWithValue(message)
   }
   }
 )

//  Update note text
export const updateNote = createAsyncThunk('notes/update', async (noteProps, thunkAPI) => {
   try {
      const {id, text} = noteProps;
      const token = thunkAPI.getState().auth.user.jwt_token
      return await notesService.updateNote(id,text, token)
   } catch(err) {
      const message = (err.response && err.response.data && err.response.data.message) ||
      err.message || err.toString()
      return thunkAPI.rejectWithValue(message)
 }
 }
)



export const notesSlice = createSlice({
   name: 'note',
   initialState,
   reducers: {
      reset: (state) => initialState,
   },
   extraReducers: (builder) => {
      builder
         .addCase(createNote.pending, (state) => {
            state.isLoading = true
         })
         .addCase(createNote.fulfilled, (state, action) => {
            state.isLoading = false
            state.isSuccess = true
            state.notes.push(action.payload.note)
         })
         .addCase(createNote.rejected, (state, action) => {
            state.isLoading = false
            state.isError = true
            state.message = action.payload
         })
         .addCase(getNotes.pending, (state) => {
            state.isLoading = true
         })
         .addCase(getNotes.fulfilled, (state, action) => {
            state.isLoading = false
            state.isSuccess = true
            state.note = null
            state.notes = action.payload.notes
         })
         .addCase(getNotes.rejected, (state, action) => {
            state.isLoading = false
            state.isError = true
            state.message = action.payload
         })
         .addCase(getNote.pending, (state) => {
            state.isLoading = true
         })
         .addCase(getNote.fulfilled, (state,action) => {
            state.isLoading = false
            state.isSuccess = true
            state.notes = []
            state.note = action.payload.note
         })
         .addCase(getNote.rejected, (state, action) => {
            state.isLoading = false
            state.isError = true
            state.message = action.payload
         })
         .addCase(deleteNote.pending, (state) => {
            state.isLoading = true
         })
         .addCase(deleteNote.fulfilled, (state, action) => {
            state.isLoading = false
            state.isSuccess = true
            if(state.notes.length) {
               state.notes = state.notes.filter((note) => note._id !== action.payload.id)
            } else {
               state.note = null
            }
           
         })
         .addCase(deleteNote.rejected, (state, action) => {
            state.isLoading = false
            state.isError = true
            state.message = action.payload
         })
         .addCase(checkNote.pending, (state) => {
            state.isLoading = true
          })
          .addCase(checkNote.fulfilled, (state, action) => {
            state.isLoading = false
            state.isSuccess = true
           const index = state.notes.findIndex((note) => {
              return note._id === action.payload.id
            })
            if(state.notes.length) {
               state.notes[index].completed = !state.notes[index].completed;
            } else {
               state.note.completed = !state.note.completed;
            }
           
          })
          .addCase(checkNote.rejected, (state, action) => {
            state.isLoading = false
            state.isError = true
            state.message = action.payload
          })
          .addCase(updateNote.pending, (state) => {
            state.isLoading = true
          })
          .addCase(updateNote.fulfilled, (state, action) => {
            state.isLoading = false
            state.isSuccess = true
           const index = state.notes.findIndex((note) => {
              return note._id === action.payload.id
            })
            if(state.notes.length) {
               state.notes[index].text = action.payload.text;
            } else {
               state.note.text = action.payload.text;
            }
            
          })
          .addCase(updateNote.rejected, (state, action) => {
            state.isLoading = false
            state.isError = true
            state.message = action.payload
          })
   }
})

export const {reset} = notesSlice.actions
export default notesSlice.reducer