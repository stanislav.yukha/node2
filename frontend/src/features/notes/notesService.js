import axios from 'axios'

const API_URL = 'http://localhost:8080/api/notes';


//get user Notes
const getNotes = async (queryParams,token) => {
   const config = {
      headers: {
         Authorization: `Bearer ${token}`
      }
   }
   const {offset, limit} = queryParams
   const response = await axios.get(API_URL, {params: {offset, limit},  headers: config.headers});

   return response.data
}

//get user Notes
const getNote = async (id, token) => {
   const config = {
      headers: {
         Authorization: `Bearer ${token}`
      }
   }
   const response = await axios.get(`${API_URL}/${id}`, config);
   return response.data
}


//Add new note
const createNote = async (noteData, token) => {
   const config = {
      headers: {
         Authorization: `Bearer ${token}`
      }
   }

   const response = await axios.post(API_URL, noteData, config);

   return response.data
}

//Delete note by id
const deleteNote = async (id, token) => {
   const config = {
      headers: {
         Authorization: `Bearer ${token}`
      }
   }
   const response = await axios.delete(`${API_URL}/${id}`, config);
   return response.data
}

// Check|uncheck 'completed' property
const checkNote = async (id, token) => {
   const config = {
      headers: {
         Authorization: `Bearer ${token}`
      }
   } 
 
   const response = await axios.patch(`${API_URL}/${id}`,'', config)
    return response.data
 }

// Update 'text' property of note by id
const updateNote = async (id, text, token) => {
   const config = {
      headers: {
         Authorization: `Bearer ${token}`
      }
   } 
   const response = await axios.put(`${API_URL}/${id}`,{text}, config)
    return response.data
 }



const notesService = {
   getNotes,
   getNote,
   createNote,
   deleteNote,
   checkNote,
   updateNote
}

export default notesService