import { configureStore } from '@reduxjs/toolkit';
import authReducer from '../features/auth/authSlice'
import notesReducer from '../features/notes/notesSlice'
import userReducer from '../features/user/userSlice'
export const store = configureStore({
  reducer: {
    auth: authReducer,
    notes: notesReducer,
    user: userReducer
  },
});
