import {FaSignInAlt, FaSignOutAlt, FaUser} from 'react-icons/fa'
import {Link, useNavigate} from 'react-router-dom'
import {useSelector, useDispatch} from 'react-redux'
import {logout, reset} from '../features/auth/authSlice'
import {reset as resetNotes} from '../features/notes/notesSlice'

function Header() {
   const navigate = useNavigate()
   const dispatch = useDispatch()
   const {user} = useSelector((state) => state.auth)

   const onLogout = () => {
      dispatch(logout());
      dispatch(reset());
      dispatch(resetNotes());
      navigate('/')
   }

  return (
    <header className='header'>
       <div className='logo'>
          <ul>
             <li>
               <Link to='/'>My notes</Link>
             </li>
             <li>
               <Link to='/new'>Add note</Link>
             </li>
             <li>
               <Link to='/about'>About me</Link>
             </li>
          </ul>
       </div>
       <ul>
          {user ? (
            <li>
                 <button className="btn btn_red" onClick={onLogout}>
                 <FaSignOutAlt /> Logout
                 </button>
           </li>
          ) : (<>
            <li>
               <Link to='/login'>
                  <FaSignInAlt /> Login
               </Link>
            </li>
            <li>
               <Link to='/register'>
                  <FaUser /> Register
               </Link>
            </li>
          </>)}
          
       </ul>
       </header>
  )
}

export default Header