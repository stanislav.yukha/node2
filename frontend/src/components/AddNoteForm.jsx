import {useEffect, useState} from 'react'
import {useNavigate} from 'react-router-dom'
import {useSelector, useDispatch} from 'react-redux'
import { toast } from 'react-toastify'
import {createNote, reset} from '../features/notes/notesSlice'
import Spinner from './Spinner'

function NoteForm() {
  
   const [text, setText] = useState('')
   const navigate = useNavigate()
   const dispatch = useDispatch()
 
   const onSubmit = (ev) => {
      ev.preventDefault()
      dispatch(createNote({text}))
      setText('')
   }
   const {user} = useSelector((state) => state.auth)
   const {isLoading,isSuccess, isError, message} = useSelector(
     (state) => state.notes
   )
   
   useEffect(()=> {
 
     if(isError) {
       toast.error(message)
     } 

     if(!user) {
       navigate('/login')
     }
     return () => {
      dispatch(reset())
     }
   }, [user, navigate,isSuccess, isError, message, dispatch])
 
   if (isLoading) {
     return <Spinner />
   }
 
  return (
    <section className="form">
       <form onSubmit={onSubmit}>
          <div className="form-group">
             <label htmlFor="text"></label>
             <input type="text" name="text" id="text" 
             value={text} onChange={(ev) => setText(ev.target.value)} 
             />
          </div>
          <div className="form-group">
             <button className="btn btn-block" type="submit">Add new note</button>
             </div>
       </form>
    </section>
  )
}

export default NoteForm