import {useEffect, useState} from 'react'
import {useNavigate} from 'react-router-dom'
import {useSelector, useDispatch} from 'react-redux'
import { toast } from 'react-toastify'
import Spinner from './Spinner'
import {getNotes, getNote} from '../features/notes/notesSlice'
import NoteItem from '../components/NoteItem'
import EditModal from '../components/EditModal'


function SearcNoteForm() {
 
   const navigate = useNavigate()
   const dispatch = useDispatch()

   let [editNote, openEdit] = useState('')
   let [closeModal, closeEdit] = useState('')
   useEffect(() => {
      if(closeModal) {
      openEdit('');
      closeEdit('')
      }
   }, [editNote, closeModal])
   
   const [formData, setFormData] = useState({
      _id: '',
      offset: 0,
      limit: 0
   })

   const {_id, offset, limit} = formData

   const onChange = (ev) => {
      setFormData((prevState) => ({
        ...prevState,
        [ev.target.name]: ev.target.value,
      }))
    }

   const onSubmit = (ev) => {
      ev.preventDefault()
      if(_id) {
         dispatch(getNote(_id))
      } else {
         dispatch(getNotes({
            offset,
            limit
         }
         ))
      }
      setFormData('')
  }

   const {user} = useSelector((state) => state.auth)
   const {notes, note, isLoading,isSuccess, isError, message} = useSelector(
     (state) => state.notes
   )

   useEffect(()=> {
 
     if(isError) {
       toast.error(message)
     } 
    
   }, [user, notes, note, navigate,isSuccess, isError, message, dispatch])
 
   if (isLoading) {
     return <Spinner />
   }
 
  return (
     <>
   
       <form className='search-form' onSubmit={onSubmit}>
          <div className="form-group">
             <input type="text" name="_id" id="_id" 
             minLength={24} defaultValue={''}
             placeholder="input note id or leave empty"
             onChange={onChange}/>
          </div>
          <div className="form-group form-group_flex">
            <label htmlFor="offset">Offset
            <input type="number" id="offset" name="offset" defaultValue={0}
            min="0" max="50" onChange={onChange} />
            </label>
            <label htmlFor="limit">Limit
            <input type="number" id="limit" name="limit" defaultValue={0}
                  min="0" max="50" onChange={onChange}/>
            </label>
            <button className="btn btn-block" type="submit">Find note(s)</button>
         </div>
       </form>

           { (notes.length) > 0 ? 
            (<div className='notes'>
                 {notes.map((noteItem) => (
                   <NoteItem key={noteItem._id} note={noteItem} noteToEdit={openEdit}/>
                 ))}
                 <EditModal noteToEdit={editNote} closeModal={closeEdit}/>
            </div>
            ) : (
               note != null ? (
               <div className='note-container'> 
                  <NoteItem key={note._id} note={note} noteToEdit={openEdit}/>
                  <EditModal noteToEdit={editNote} closeModal={closeEdit}/>
               </div>
               ) :
               ('')
            ) }
   
    </>
  )
}

export default SearcNoteForm