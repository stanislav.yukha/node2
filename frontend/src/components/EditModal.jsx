import {useDispatch} from 'react-redux'
import {updateNote} from '../features/notes/notesSlice'

function EditModal(props) {

   const dispatch = useDispatch()

   let newTextValue = '';

  const onSave = () => {
     props.closeModal(true)
     dispatch(updateNote({id: props.noteToEdit._id, text: newTextValue}))
   }

   const onCancel = () => {
      props.closeModal(true)
   }
  return (
    <div className={props.noteToEdit ? 'edit-modal' : 'edit-modal_hide'}>
       <div className="edit-modal__container">
         <textarea 
            className="edit-modal__text"
            name="text" id="text"
            cols="30"
            rows="5"
            defaultValue={props.noteToEdit.text}
            onChange={(ev) => newTextValue = ev.target.value}
          ></textarea>
         <div className="edit-modal__buttons">
            <button className="btn" onClick={onSave}>Save</button>
            <button className="btn btn_red" onClick={onCancel}>Cancel</button>
         </div>
         
       </div>
    </div>

  )
}

export default EditModal