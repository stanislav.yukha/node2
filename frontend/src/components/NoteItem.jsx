import {useDispatch} from 'react-redux'
import {deleteNote} from '../features/notes/notesSlice'
import {checkNote} from '../features/notes/notesSlice'
import {FaPencilAlt} from 'react-icons/fa'
import moment from 'moment'


function NoteItem(props) {
  const dispatch = useDispatch();
 
  return (
    <div className="note">
       <h3 className="note__text">{props.note.text}</h3>
       <div className="note__date">
          <b>Created at: </b>{moment(props.note.createdDate).format('M D YYYY, h:mm:s a')}
       </div>
       <div className='note__completed'>
       <input type="checkbox" name='completed' id="completed" 
       defaultChecked={props.note.completed} onClick={() => dispatch(checkNote(props.note._id))}/>
        <label onClick={() => dispatch(checkNote(props.note._id))} htmlFor="completed"> Completed</label>
      </div>
      
      <button onClick={() => props.noteToEdit(props.note)} className='note__edit' title='edit'>
       <FaPencilAlt />
      </button>
      <button onClick={() => dispatch(deleteNote(props.note._id))} className="note__close" title='delete'>X</button>
    </div>
    
  )
}

export default NoteItem