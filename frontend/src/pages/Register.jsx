import { useState, useEffect } from 'react'
import { FaUser } from 'react-icons/fa'
import { useSelector, useDispatch } from 'react-redux'
import { useNavigate } from 'react-router-dom'
import { toast } from 'react-toastify'
import { register, reset } from '../features/auth/authSlice'
import Spinner from '../components/Spinner'

function Register() {
   const [formData, setFormData] = useState({
      username: '',
      password: '',
      password2: ''

   })

   const {username, password, password2} = formData
   const navigate = useNavigate()
   const dispatch = useDispatch()

   const {user, isLoading, isError, isSuccess, message} = useSelector(
     (state) => state.auth)

   useEffect(()=> {
      if(isError) {
        toast.error(message)
      }

      if(isSuccess || user) {
        navigate('/')
      }

      dispatch(reset())
   }, [user, isError, isSuccess, message, navigate, dispatch])
   
   const onChange = (ev) => {
     setFormData((prevState) => ({
       ...prevState,
       [ev.target.name]: ev.target.value,
     }))
   }
   const onSubmit = (ev) => {
      ev.preventDefault()
      if(password !== password2) {
        toast.error('Passwords do not match')
      } else {
        const userData = {
          username,
          password
        }

        dispatch(register(userData));
      }
  }

  if (isLoading) {
    return <Spinner />
  }

  return (
    <>
    <section className="heading">
      <h1>
        <FaUser /> Register
      </h1>
      <p>Please create an account</p>
    </section>
    <section className="form">
      <form onSubmit={onSubmit}>
        <div className="form-group">
          <input type="text" className="form-control"
          id='username' name='username' value={username}
          placeholder='Enter your name' onChange={onChange} 
          required/>
        </div>
        <div className="form-group">
          <input type="password" className="form-control"
          id='password' name='password' value={password}
          placeholder='Enter your password' onChange={onChange} 
          required/>
        </div>
        <div className="form-group">
          <input type="password" className="form-control"
          id='password2' name='password2' value={password2}
          placeholder='Repeat your password' onChange={onChange} 
          required/>
        </div>
        <div className="form-group">
          <button type='submit' className='btn btn-block'>Submit</button>
        </div>
      </form>
    </section>
    
    </>
  )
}

export default Register