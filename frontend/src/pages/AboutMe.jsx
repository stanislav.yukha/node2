import {useState, useEffect} from 'react'
import {useNavigate} from 'react-router-dom'
import {useSelector, useDispatch} from 'react-redux'
import {getMyInfo,changeMyPassword, deleteMe, reset} from '../features/user/userSlice'
import {reset as resetNotes} from '../features/notes/notesSlice'
import { logout } from '../features/auth/authSlice'
import { toast } from 'react-toastify'
import moment from 'moment'
import Spinner from '../components/Spinner'



function AboutMe() {

  

   const [formData, setFormData] = useState({
      oldpassword: '',
      newpassword: '',
      newpassword2: ''
   })
   const {oldpassword, newpassword, newpassword2} = formData

   const navigate = useNavigate()
   const dispatch = useDispatch();

   const user = useSelector((state) => state.auth)
   const {userInfo, isError, isLoading, message} =  useSelector((state) => state.user)
 
   useEffect(()=> {

      if(isError) {
      toast.error(message)
      }

      if(!user.user) {
        navigate('/login')
      } else {
        dispatch(getMyInfo())
      }

      return () => {
      dispatch(reset())
      }
      
   }, [user, navigate, isError, message, dispatch])

   if (isLoading) {
      return <Spinner />
   }

   const onChange = (ev) => {
      setFormData((prevState) => ({
         ...prevState,
         [ev.target.name]: ev.target.value,
      }))
   }

   const onSubmit = (ev) => {
      ev.preventDefault()
      if(newpassword !== newpassword2) {
        toast.error('Password do not match')
      } else {
        const userData = {
         oldPassword: oldpassword,
         newPassword: newpassword
        }
        dispatch(changeMyPassword(userData));
        setFormData('')
      }
  }

  const onDelete = () => {
    const agree = window.confirm('Are you sure?')
    if (agree) {
      dispatch(deleteMe())
      dispatch(logout())
      dispatch(resetNotes());
      dispatch(reset())
      navigate('/')
    }
   
 }


  return (
    <div className='about-me'>
       <div className="about-me__info">
         <h3><span className='about-me__title'>User id:</span> {userInfo?._id}</h3>
         <h3><span className='about-me__title'>Username:</span> {userInfo?.username}</h3>
         <h3><span className='about-me__title'>Register date:</span> {moment(userInfo?.createdDate).format('MMMM Do YYYY, h:mm:ss a')}</h3>
       </div>
       <h3>Change password: </h3>
       <div className="about-me__form">
       <form onSubmit={onSubmit}>
       <div className="form-group">
          <input type="password" className="form-control"
          id='oldpassword' name='oldpassword' value={oldpassword}
          placeholder='Enter old password' onChange={onChange} 
          required/>
        </div>
        <div className="form-group">
          <input type="password" className="form-control"
          id='newpassword' name='newpassword' value={newpassword}
          placeholder='Enter new password' onChange={onChange}
          required />
        </div>
        <div className="form-group">
          <input type="password" className="form-control"
          id='newpassword2' name='newpassword2' value={newpassword2}
          placeholder='Repeat new password' onChange={onChange} 
          required/>
        </div>
        <div className="form-group">
          <button type='submit' className='btn btn-block'>Submit</button>
        </div>
      </form>
       </div>
       <h3>Delete my account:</h3>
       <button className="btn btn_red" onClick={() => onDelete()}>Delete</button>
    
    </div>
  )
}

export default AboutMe