import {useEffect} from 'react'
import {useNavigate} from 'react-router-dom'
import {useSelector} from 'react-redux'
import SearchNoteForm from '../components/SearchNoteForm'

function Dashboard() {

  const navigate = useNavigate()

  const {user} = useSelector((state) => state.auth)

  useEffect(()=> {

    if(!user) {
      navigate('/login')
    } 

  
  }, [user, navigate])

  return (
    <>
      <section className="heading">
        <h3>Welcome to your notes {user && user.username}</h3>
      </section>
      <section className="content">
       <SearchNoteForm />
      </section>
  
    </>
  )
}

export default Dashboard