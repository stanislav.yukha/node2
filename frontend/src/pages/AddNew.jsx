import AddNoteForm from '../components/AddNoteForm'

function AddNew() {
  return (
    <>
       <AddNoteForm />
    </>
  )
}

export default AddNew