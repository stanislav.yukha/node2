import { BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import { ToastContainer } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import Header from './components/Header';
import Dashboard from './pages/Dashboard';
import Login from './pages/Login';
import Register from './pages/Register';
import AddNew from './pages/AddNew';
import AboutMe from './pages/AboutMe';


function App() {
  return (
    <>
      <Router>
        <div className="container">
          <Header />
         <Routes>
           <Route path='/' element={<Dashboard />}/>
           <Route path='/new' element={<AddNew />} />
           <Route path='/about' element={<AboutMe />} />
           <Route path='/login' element={<Login />}/>
           <Route path='/register' element={<Register />}/>
         </Routes>
        </div>
      </Router>
      <ToastContainer />
    </>
  );
}

export default App;
