const express = require('express');
const {Router} = express;
const router = new Router();
const {registerUser, loginUser} = require('../controllers/authController');

router.route('/register').post(registerUser);
router.route('/login').post(loginUser);

module.exports = router;
