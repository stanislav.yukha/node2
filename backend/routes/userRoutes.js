const express = require('express');
const {Router} = express;
const router = new Router();
const {
  getMyInfo,
  deleteMe,
  changeMyPass} = require('../controllers/userController');
const protect = require('../middleware/authMiddleware');

router.get('/', protect, getMyInfo);
router.delete('/', protect, deleteMe);
router.patch('/', protect, changeMyPass);

module.exports = router;
