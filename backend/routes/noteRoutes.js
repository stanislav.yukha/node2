const express = require('express');
const {Router} = express;
const router = new Router();
const {
  getNotes,
  addNote,
  getNote,
  updateNote,
  checkNote,
  deleteNote} = require('../controllers/noteController');
const protect = require('../middleware/authMiddleware');

router.route('/')
    .get(protect, getNotes)
    .post(protect, addNote);
router.route('/:id')
    .get(protect, getNote)
    .put(protect, updateNote)
    .patch(protect, checkNote)
    .delete(protect, deleteNote);


module.exports = router;
