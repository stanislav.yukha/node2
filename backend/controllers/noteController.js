const asyncHadler = require('express-async-handler');
const Note = require('../models/noteModel');
const User = require('../models/userModel');


// @desc Get notes
// @route GET /api/notes
// @access Private
const getNotes = asyncHadler(async (req, res) => {
  const {offset, limit} = req.query;
  const notes = await Note.find({userId: req.user.id})
      .skip(offset || 0)
      .limit(limit || 0);
  const count = notes.length;
  res.status(200).json({
    offset,
    limit,
    count,
    notes: notes,
  });
});

// @desc Add note
// @route POST /api/notes
// @access Private
const addNote = asyncHadler(async (req, res) => {
  if (!req.body.text) {
    res.status(400);
    throw new Error('No \'text\' parameter provided or it is empty');
  }
  const note = await Note.create({
    text: req.body.text,
    userId: req.user._id,
  });
  res.status(200).json({
    message: 'Success',
    note,
  });
});

// @desc Get note by id
// @route GET /api/notes/:id
// @access Private
const getNote = asyncHadler(async (req, res) => {
  const userId = req.user.id;
  const note = await Note.findOne({_id: req.params.id, userId});
  if (!note) {
    res.status(400);
    throw new Error('Note with such id not found');
  }
  res.status(200).json({note: note});
});

// @desc Update note by id
// @route PUT /api/notes/:id
// @access Private
const updateNote = asyncHadler(async (req, res) => {
  const note = await Note.findById(req.params.id);
  const user = await User.findById(req.user.id);
  if (!note) {
    res.status(400);
    throw new Error(`Note with id ${req.params.id} not found`);
  }
  if (!user) {
    res.status(401);
    throw new Error('User not found');
  }
  if (note.userId.toString() !== user.id) {
    res.status(401);
    throw new Error('User not authorized');
  }
  if (!req.body.text) {
    res.status(400);
    throw new Error(`Parameter 'text' should not be empty`);
  }
  await Note.findByIdAndUpdate(req.params.id, {text: req.body.text});
  res.status(200).json({
    message: 'Success',
    id: req.params.id,
    text: req.body.text,
  } );
});

// @desc Change check state of note by id
// @route PATCH /api/notes/:id
// @access Private
const checkNote = asyncHadler(async (req, res) => {
  const note = await Note.findById(req.params.id);
  const user = await User.findById(req.user.id);
  if (!note) {
    res.status(400);
    throw new Error(`Note with id ${req.params.id} not found`);
  }
  if (!user) {
    res.status(401);
    throw new Error('User not found');
  }
  if (note.userId.toString() !== user.id) {
    res.status(401);
    throw new Error('User not authorized');
  }
  await Note.findByIdAndUpdate(req.params.id, {completed: !note.completed});
  res.status(200).json({
    message: 'Success',
    id: req.params.id,
  });
});

// @desc Delete note by id
// @route DELETE /api/notes/:id
// @access Private
const deleteNote = asyncHadler(async (req, res) => {
  const note = await Note.findById(req.params.id);
  const user = await User.findById(req.user.id);
  if (!note) {
    res.status(400);
    throw new Error(`Note with id ${req.params.id} not found`);
  }
  if (!user) {
    res.status(401);
    throw new Error('User not found');
  }
  if (note.userId.toString() !== user.id) {
    res.status(401);
    throw new Error('User not authorized');
  }
  await Note.deleteOne({_id: req.params.id, userId: req.user.id});
  res.status(200).json({
    message: 'Success',
    id: req.params.id,
  });
});


module.exports = {
  getNotes,
  addNote,
  getNote,
  updateNote,
  checkNote,
  deleteNote,
};

