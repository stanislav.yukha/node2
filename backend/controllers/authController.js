const asyncHadler = require('express-async-handler');
const jwt = require('jsonwebtoken');
const bcrypt = require('bcryptjs');
const User = require('../models/userModel');
const Credentials = require('../models/credentialsModel');

// @desc Get notes
// @route POST /api/auth/register
// @access Public
const registerUser = asyncHadler(async (req, res) => {
  const username = req.body.username;
  const password = req.body.password;
  if (!username || !password) {
    res.status(400);
    throw new Error('Please check all fields');
  }
  const userExists = await User.findOne({username: username});
  if (userExists) {
    res.status(400);
    throw new Error(`Username '${username}' already exists`);
  }
  const salt = await bcrypt.genSalt(10);
  const hashPassword = await bcrypt.hash(password, salt);
  const user = await User.create({
    username: username,
  });
  const credentials = await Credentials.create({
    username: username,
    password: hashPassword,
  });
  if (user && credentials) {
    res.status(200).json({
      message: 'Success',
      username: user.username,
      jwt_token: generateToken(user._id),
    });
  } else {
    res.status(400);
    throw new Error('Invalid user data');
  }
});

// @desc Add note
// @route POST /api/auth/login
// @access Public
const loginUser = asyncHadler(async (req, res) => {
  const username = req.body.username;
  const password = req.body.password;
  const user = await User.findOne({username});
  const credentials = await Credentials.findOne({username});
  if (user && ( await bcrypt.compare(password, credentials.password))) {
    res.status(200).json({
      message: 'Success',
      username: user.username,
      jwt_token: generateToken(user._id),
    });
  } else {
    res.status(400);
    throw new Error('Invalid user credentials');
  }
});

const generateToken = (id) => {
  return jwt.sign({id}, process.env.JWT_SECRET, {
    expiresIn: '10d',
  });
};

module.exports = {
  registerUser,
  loginUser,
};
