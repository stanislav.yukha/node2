const asyncHadler = require('express-async-handler');
const User = require('../models/userModel');
const Note = require('../models/noteModel');
const Credentials = require('../models/credentialsModel');
const bcrypt = require('bcryptjs');


// @desc Get notes
// @route GET /api/users/me
// @access Private
const getMyInfo = asyncHadler(async (req, res) => {
  if (!req.user) {
    res.status(400);
    throw new Error('User doesn`t exist');
  } else {
    const {_id, username, createdDate} = await User.findById(req.user.id);
    res.status(200).json({
      user: {
        _id,
        username,
        createdDate,
      }});
  };
});

// @desc Get notes
// @route DELETE /api/users/me
// @access Private
const deleteMe = asyncHadler(async (req, res) => {
  if (!req.user) {
    res.status(400);
    throw new Error('User doesn`t exist');
  } else {
    const {_id, username} = await User.findById(req.user.id);
    await User.deleteOne({_id});
    await Credentials.deleteOne({username});
    await Note.deleteMany({userId: _id}, function(err, _) {
      if (err) {
        return console.log(err);
      }
    });
    res.status(200).json({message: 'Success'});
  }
});

// @desc Get notes
// @route PATCH /api/users/me
// @access Private
const changeMyPass = asyncHadler(async (req, res) => {
  if (!req.user) {
    res.status(400);
    throw new Error('User doesn`t exist');
  } else {
    const {oldPassword, newPassword} = req.body;
    const {username} = await User.findById(req.user.id);
    const oldpasswordDB = (await Credentials.findOne({username})).password;
    if (!(await bcrypt.compare(oldPassword, oldpasswordDB))) {
      res.status(400);
      throw new Error('Wrong old password');
    } else if (!newPassword) {
      res.status(400);
      throw new Error('New password should not be empty');
    } else if (oldPassword === newPassword) {
      res.status(400);
      throw new Error('New password should be different from old one');
    } else {
      const salt = await bcrypt.genSalt(10);
      const hashPassword = await bcrypt.hash(newPassword, salt);
      await Credentials.findOneAndUpdate({username}, {password: hashPassword});
      res.status(200).json({message: 'Successfully changed user password'});
    }
  }
});


module.exports = {
  getMyInfo,
  deleteMe,
  changeMyPass,
};
