const mongoose = require('mongoose');
const {Schema} = mongoose;
const userSchema = new Schema({
  username: {
    type: String,
    required: [true, 'Please add username'],
  },
}, {
  timestamps: {createdAt: 'createdDate', updatedAt: false},
},
);

module.exports = mongoose.model('User', userSchema);
