const mongoose = require('mongoose');
const {Schema} = mongoose;
const credentialsSchema = new Schema({
  username: {
    type: String,
    required: [true, 'Please add username'],
  },
  password: {
    type: String,
    required: [true, 'Please add password'],
  },
});

module.exports = mongoose.model('Credentials', credentialsSchema);
