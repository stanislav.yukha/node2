const mongoose = require('mongoose');
const {Schema} = mongoose;
const noteSchema = new Schema({
  userId: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'User',
  },
  completed: {
    type: Boolean,
    default: false,
  },
  text: {
    type: String,
    required: [true, 'Please add a text value'],
  },
}, {
  timestamps: {createdAt: 'createdDate', updatedAt: false},
},
);

module.exports = mongoose.model('Note', noteSchema);
